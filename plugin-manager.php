<?php

/*
  Plugin Name: Plugin Manager
  Description: Plugin Manager
  Version: 0.1
  Author: Aleksandr Zemlianoi
  License: GPLv2 or later
 */
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

define('ZAI_PM_PLUGIN_DIR', plugin_dir_path(__FILE__));

//register_activation_hook(__FILE__, array('PluginManager', 'pluginActivation'));
//register_deactivation_hook(__FILE__, array('PluginManager', 'pluginDeactivation'));

require_once( ZAI_PM_PLUGIN_DIR . 'plugin-manager-controller.php' );

add_action('network_admin_menu', array('PluginManager', 'registerNetworkSubPage'));

add_action('wp_ajax_zai_pm_get_table', array('PluginManager', 'getTablesPlugin'));
add_action('wp_ajax_zai_pm_activate_plugin', array('PluginManager', 'activatePlugin'));
add_action('wp_ajax_zai_pm_deactivate_plugin', array('PluginManager', 'deactivatePlugin'));
