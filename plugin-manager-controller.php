<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PluginManager {

	public static function registerNetworkSubPage() {
		add_submenu_page('settings.php', 'Plugin Manager', 'Plugin Manager', 'manage_options', 'pluginmanager', array('PluginManager', 'pluginManagerPage'));
	}

	public static function pluginManagerPage() {
		?>
		<script>
			var search = '';

			function getTables() {
				var data = {action: 'zai_pm_get_table', search: search};
				jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
					jQuery('#manage-plugin').html(response);
				});
			}

			function deactivatePlugin(plugin, wide, blog_id) {
				var data = {
					plugin: plugin,
					action: 'zai_pm_deactivate_plugin',
					wide: wide,
					blog_id: blog_id
				}
				jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
					getTables();
				});
			}

			function activatePlugin(plugin, wide, blog_id) {
				var data = {
					plugin: plugin,
					action: 'zai_pm_activate_plugin',
					wide: wide,
					blog_id: blog_id
				}
				jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
					getTables();
				});
			}

			function makeSearch(s) {
				if (search == s) {
					getTables()
				}

			}

			getTables();
			setInterval('getTables();', 5000);

			jQuery(document).on('click', '.updateLink', function() {
				jQuery.get(jQuery(this).attr('href'), function(resp) {
					if (resp != '') {
						var html = jQuery(resp).find('.wrap');
						getTables();
						alert(jQuery(html).find('p').eq(4).text() + ' ' + jQuery(html).find('p').eq(5).text());
					}
				});
				return false;
			});

			jQuery(document).on('keyup', '#plugin-search-input', function() {
				var s = this.value;
				search = s;
				setTimeout('makeSearch("' + s + '")', 300);
			});

		</script>
		<br />
		<p class="search-box">
			<label class="screen-reader-text" for="plugin-search-input">Search Installed Plugins:</label>
			<input type="search" id="plugin-search-input" name="s" value="">
		</p>
		<div id="manage-plugin" class="wrap"></div>
		<?php
	}

	public static function activatePlugin() {
		if ($_POST['blog_id'] != 0) {
			switch_to_blog($_POST['blog_id']);
		}
		$plugin = $_POST['plugin'];

		$network_wide = $_POST['wide'] ? true : false;
		if ($network_wide) {
			$current = get_site_option('active_sitewide_plugins', array());
		} else {
			$current = get_option('active_plugins', array());
		}
		$_GET['networkwide'] = 1;

		do_action('activate_plugin', $plugin, $network_wide);
		do_action('activate_' . $plugin, $network_wide);

		if ($network_wide) {
			$current[$plugin] = time();
			update_site_option('active_sitewide_plugins', $current);
		} else {
			$current[] = $plugin;
			sort($current);
			update_option('active_plugins', $current);
		}

		do_action('activated_plugin', $plugin, $network_wide);
	}

	public static function deactivatePlugin() {
		if ($_POST['blog_id'] != 0) {
			switch_to_blog($_POST['blog_id']);
		}

		$network_wide = $_POST['wide'] ? true : false;

		if (is_multisite()) {
			$network_current = get_site_option('active_sitewide_plugins', array());
		}

		$current = get_option('active_plugins', array());

		$do_blog = $do_network = false;
		$plugin = plugin_basename(trim($_POST['plugin']));

		$network_deactivating = false !== $network_wide && is_plugin_active_for_network($plugin);

		do_action('deactivate_plugin', $plugin, $network_wide);

		if (false !== $network_wide) {
			if (is_plugin_active_for_network($plugin)) {
				$do_network = true;
				unset($network_current[$plugin]);
			}
		}

		if (true !== $network_wide) {
			$key = array_search($plugin, $current);
			if (false !== $key) {
				$do_blog = true;
				unset($current[$key]);
			}
		}

		do_action('deactivate_' . $plugin, $network_deactivating);
		do_action('deactivated_plugin', $plugin, $network_deactivating);

		if ($do_blog) {
			update_option('active_plugins', $current);
		}

		if ($do_network) {
			update_site_option('active_sitewide_plugins', $network_current);
		}
	}

	public static function getTablesPlugin() {
		$all_plugins = get_plugins();
		$plugins = get_site_transient('update_plugins');
		$search = strtolower(trim($_POST['search']));
		?>

		<h2>Network Plugins</h2>
		<br /><br />
		<table class="wp-list-table widefat plugins">
			<thead>
				<tr>
					<th scope="col" id="name" class="manage-column column-name" style="">Plugin</th>
					<th scope="col" id="description" class="manage-column column-description" style="">Description</th> 
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th scope="col" class="manage-column column-name" style="">Plugin</th>
					<th scope="col" class="manage-column column-description" style="">Description</th> 
				</tr>
			</tfoot>
			<tbody class="the-list">
				<?php
				foreach ($all_plugins as $k => $v) {
					if ($v['Name'] == 'Plugin Manager') {
						continue;
					}
					if ($search != '' && strpos(strtolower($v['Name']), $search) === false) {
						continue;
					}

					$upd = 0;
					$active = 0;
					if (isset($plugins->response[$k])) {
						$upd = 1;
					}
					if (is_plugin_active_for_network($k)) {
						$active = 1;
					}
					?>
					<tr id="<?= $v['TextDomain'] ?>" class="<?= ($active) ? "active" : "inactive" ?> <?= $upd ? "update" : "" ?>">
						<td class="plugin-title">
							<strong><?= $v['Name'] ?></strong>
							<div class="row-actions visible">
								<span class="activate">
									<?php if ($active) { ?>
										<a href="#" title="Deactivate this plugin" class="edit" onclick="deactivatePlugin('<?= $k ?>', 1, 0);
																return false;">
											Network Deactivate
										</a>
									<?php } else { ?>
										<a href="#" title="Activate this plugin" class="edit" onclick="activatePlugin('<?= $k ?>', 1, 0);
																return false;">
											Network Activate
										</a>
									<?php } ?>
								</span>
							</div>
						</td>
						<td class="column-description desc">
							<div class="plugin-description">
								<p><?= $v['Description'] ?></p>
							</div>
							<div class="inactive second plugin-version-author-uri">
								Version <?= $v['Version'] ?> | 
								By <a href="<?= $v['AuthorURI'] ?>"><?= $v['AuthorName'] ?></a>
							</div>
						</td>
					</tr>
					<?php if ($upd) { ?>
						<tr class="plugin-update-tr">
							<td colspan="3" class="plugin-update colspanchange">
								<div class="update-message">
									There is a new version of <?= $v['Name'] ?> available. 
									<a class="updateLink" href="<?= wp_nonce_url(self_admin_url('update.php?action=upgrade-plugin&plugin=') . $k, 'upgrade-plugin_' . $k) ?>">Update now</a>.
								</div>
							</td>
						</tr>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>
		<?php
		$blog_list = get_blog_list(0, 'all');
		foreach ($blog_list as $blog) {
			switch_to_blog($blog['blog_id']);
			?>
			<br /><br />
			<h2>ID: <?= $blog['blog_id'] ?> | Name: <?= $blog['domain'] . $blog['path'] ?></h2>
			<br /><br />
			<table class="wp-list-table widefat plugins">
				<thead>
					<tr>
						<th scope="col" id="name" class="manage-column column-name" style="">Plugin</th>
						<th scope="col" id="description" class="manage-column column-description" style="">Description</th> 
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th scope="col" class="manage-column column-name" style="">Plugin</th>
						<th scope="col" class="manage-column column-description" style="">Description</th> 
					</tr>
				</tfoot>
				<tbody id="the-list">
					<?php
					foreach ($all_plugins as $k => $v) {
						if (is_plugin_active_for_network($k)) {
							continue;
						}
						if ($search != '' && strpos(strtolower($v['Name']), $search) === false) {
							continue;
						}

						$upd = 0;
						$active = 0;
						if (isset($plugins->response[$k])) {
							$upd = 1;
						}
						if (is_plugin_active($k)) {
							$active = 1;
						}
						?>
						<tr id="<?= $v['TextDomain'] ?>" class="<?= ($active) ? "active" : "inactive" ?> <?= $upd ? "update" : "" ?>">
							<td class="plugin-title">
								<strong><?= $v['Name'] ?></strong>
								<div class="row-actions visible">
									<span class="activate">
										<?php if ($active) { ?>
											<a href="#" title="Deactivate this plugin" class="edit" onclick="deactivatePlugin('<?= $k ?>', 0, '<?= $blog['blog_id'] ?>');
																		return false;">
												Deactivate
											</a>
										<?php } else { ?>
											<a href="#" title="Activate this plugin" class="edit" onclick="activatePlugin('<?= $k ?>', 0, '<?= $blog['blog_id'] ?>');
																		return false;">
												Activate
											</a>
										<?php } ?>
									</span>
								</div>
							</td>
							<td class="column-description desc">
								<div class="plugin-description">
									<p><?= $v['Description'] ?></p>
								</div>
								<div class="inactive second plugin-version-author-uri">
									Version <?= $v['Version'] ?> | 
									By <a href="<?= $v['AuthorURI'] ?>"><?= $v['AuthorName'] ?></a>
								</div>
							</td>
						</tr>
						<?php if ($upd) { ?>
							<tr class="plugin-update-tr">
								<td colspan="3" class="plugin-update colspanchange">
									<div class="update-message">
										There is a new version of <?= $v['Name'] ?> available. 
										<a href="#">Update now</a>.
									</div>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
		<?php } ?>
		<?php
		die;
	}

	public static function pluginActivation() {
		
	}

	public static function pluginDeactivation() {
		
	}

}
